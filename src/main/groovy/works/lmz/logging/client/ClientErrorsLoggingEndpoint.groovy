package works.lmz.logging.client

import groovy.transform.CompileStatic
import works.lmz.common.jsresource.ApplicationResource
import works.lmz.common.jsresource.ResourceScope
import works.lmz.common.stereotypes.SingletonBean

/**
 * To make client-error-logging endpoint available on the client.
 *
 * author: Irina Benediktovich - http://plus.google.com/+IrinaBenediktovich
 */
@CompileStatic
@SingletonBean
class ClientErrorsLoggingEndpoint implements ApplicationResource {

	/**
	 * System property that contains web app context
	 */
	private static final String WEBAPP_CONTEXT = "webapp.context";

	/**
	 * @return the context path
	 */
	protected String getContextPath() {
		String path = System.getProperty(WEBAPP_CONTEXT);
		if (path == "/" || path == null) {
			return ""
		} else {
			if (path.endsWith('/'))
				path = path.substring(0, path.length()-1)
			return path;
		}
	}

	/**
	 * @return this is a global application resource
	 */
	@Override
	ResourceScope getResourceScope() {
		return ResourceScope.Global;
	}

	/**
	 * @return the map of endpoints
	 */
	@Override
	Map<String, Object> getResourceMap() {
		return ['logging': [url: getContextPath()+"/clienterrorlogger/log"] as Object]
	}
}
